import 'package:flutter/material.dart';
import 'package:primera_app/UI/screen/card.dart';

//Esta sería la clase base de donde empieza el proyecto
//Acá estamos usando una herencia con la palabra extends
class MyApp extends StatelessWidget {
  //Variables para reutilizar en el diseño
  //Final es para que se encapsule una varibla (no se puede usar por nadie más
  //que él mismo)
  final double iconSize = 40.0;
  //Acá le damos la altura y color a la letra
  final TextStyle textStyle = TextStyle(color: Colors.yellow, fontSize: 30.0);
  //override = sobreescribir
  @override
  //BuildContext sirve para reconocer el context de cada widget
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //Se identifica el titulo en la ventana widget
        title: Text("Soy un Stateless Widget"),
      ),
      //Documentación para el siguiente código
      //https://esflutter.dev/docs/development/ui/widgets/basics -> la opción
      //de Column y de Card
      body: Container(
        child: Column(
          //Acá es donde estableces el tamaño a ocupar en la pantalla
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            //Este código íria aquí
            //MyCard es la clase que se definio más abajo
            MyCard(
              title: Text(
                "Queremos mucho a flutter",
                style: textStyle,
              ),
              icon: Icon(
                Icons.favorite,
                color: Colors.redAccent,
                size: iconSize,
              ),
            ),
            MyCard(
              title: Text(
                "Vamos a saber mucho de flutter",
                style: textStyle,
              ),
              icon: Icon(
                Icons.access_alarm,
                color: Colors.amber,
                size: iconSize,
              ),
            ),
            MyCard(
              title: Text(
                "A estudiar mucho de flutter",
                style: textStyle,
              ),
              icon: Icon(
                Icons.thumb_up,
                color: Colors.blue,
                size: iconSize,
              ),
            )
          ],
        ),
      ),
    );
  }
}

//Este código iría aquí
//Esta sería la no forma correcta para reutilizar código
/*Text(
              "Prueba",
              //Esta sería la no correcta forma para reutilizar codigo
              //style: TextStyle(color: Colors.grey, fontSize: 30.0),
            ),
            Icon(
              Icons.favorite,
              //Le damos un color al icono
              color: Colors.redAccent,
              //Le damo un tamaño al icono
              //Esta sería la forma no correcta para reutilizar código size: 40.0,
            )**/

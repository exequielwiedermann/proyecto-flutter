import 'package:flutter/material.dart';

//Vamos a pasar del widget Card a esta este Widget MyCard la repetitiva
class MyCard extends StatelessWidget {
  //Los variables que vamos a recibir para hacer la repetiva
  final Widget title;
  final Widget icon;

  //Constructor
  MyCard({this.title, this.icon});

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Container(
            //Establecemos el contorno con padding: EdgeInsets
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[this.title, this.icon],
            )));
  }
}

import 'package:flutter/material.dart';
import 'package:primera_app/UI/app.dart';

void main() {
  runApp(MaterialApp(home: MyApp()));
}

//Para lo siguiente razonar la siguiente red conceptual
//Card->Text->Icon
//Text->Container para los margen
//Text->Style
//Text->Otro Column? (esto es porque si quiero agregar otro container)
//Icon->Otro Column? (esto es porque si quiero agregar otro container)
